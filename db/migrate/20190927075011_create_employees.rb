class CreateEmployees < ActiveRecord::Migration[6.0]
  def change
    create_table :employees do |t|
      t.string :name
      t.text :address
      t.float :salary
      t.integer :branch_id

      t.timestamps
    end
  end
end
