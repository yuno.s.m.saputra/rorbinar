class CreateLaptops < ActiveRecord::Migration[6.0]
  def change
    create_table :laptops do |t|
      t.string :brand
      t.string :tipe
      t.integer :employee_id
      t.integer :is_active

      t.timestamps
    end
  end
end
